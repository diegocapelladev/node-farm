const fs = require('fs')
const http = require('http')
const url = require('url')
const replaceTemplate = require('./modules/replaceTemplate')

const overviewTemplate = fs.readFileSync(`${__dirname}/templates/overview.html`, 'utf-8')
const productTemplate = fs.readFileSync(`${__dirname}/templates/product.html`, 'utf-8')
const cardTemplate = fs.readFileSync(`${__dirname}/templates/card.html`, 'utf-8')

const data = fs.readFileSync(`${__dirname}/dev-data/data.json`, 'utf-8')
const dataObj = JSON.parse(data)

const server = http.createServer((req, res) => {
  const { query, pathname } = url.parse(req.url, true)

  if (pathname === '/' || pathname === '/overview') {
    res.writeHead(200, { 'Content-type': 'text/html' })
    const cardHtml = dataObj.map(items => replaceTemplate(cardTemplate, items)).join('')
    const output = overviewTemplate.replace('{%PRODUCT_CARDS%}', cardHtml)
    res.end(output)

  } else if (pathname === '/product') {
    res.writeHead(200, { 'Content-type': 'text/html' })
    const product = dataObj[query.id]
    const output = replaceTemplate(productTemplate, product)
    res.end(output)

  } else if(pathname === '/api') {
    res.writeHead(200, { 'Content-Type': 'application/json' })
    res.end(data)

  } else {
    res.writeHead(404, {'Content-Type': 'text/html'})
    res.end('<div style="width: 100%; padding-top: 50px; margin: 0 auto; text-align: center;"><h1>Page not found!</h1></div>')
  }
})

server.listen(3000, () => {
  console.log('Server is running.')
})